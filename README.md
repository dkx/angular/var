# DKX/Angular/Var

Angular directive for declaring template variables

## Installation

```bash
$ npm install --save @dkx/ng-var
```

or with yarn

```bash
$ yarn add @dkx/ng-var
```

## Usage

**Module:**

```typescript
import {NgModule} from '@angular/core';
import {VarModule} from '@dkx/ng-var';

@NgModule({
    imports: [
        VarModule,
    ],
})
export class AppModule {}
```

**Template:**

```html
<div *dkxVar="{a: 1, b: 2, c: 3}; let data">
    {{ data | json }}
</div>
```
