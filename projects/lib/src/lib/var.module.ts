import {NgModule} from '@angular/core';
import {VarDirective} from './var.directive';


@NgModule({
	declarations: [
		VarDirective,
	],
	exports: [
		VarDirective,
	],
})
export class VarModule {}
