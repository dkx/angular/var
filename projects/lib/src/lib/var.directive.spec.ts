import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Component} from '@angular/core';

import {VarDirective} from './var.directive';


describe('VarDirective', () => {
	let component: TestComponent;
	let fixture: ComponentFixture<TestComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				TestComponent,
				VarDirective,
			],
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TestComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create local variable', () => {
		const el: HTMLElement = fixture.debugElement.nativeElement;
		expect(el.innerText).toBe('Hello world');
	});
});

@Component({
	template: '<ng-container *dkxVar="message.text; let msg">{{ msg }}</ng-container>',
})
class TestComponent
{
	public readonly message: {text: string} = {
		text: 'Hello world',
	};
}
