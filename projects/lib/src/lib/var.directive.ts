import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';


declare interface VarDirectiveContext<TData>
{
	$implicit: TData;
}

@Directive({
	selector: '[dkxVar]',
})
export class VarDirective<TData>
{
	constructor(
		private readonly viewContainerRef: ViewContainerRef,
		private readonly templateRef: TemplateRef<VarDirectiveContext<TData>>,
	) {}

	@Input()
	public set dkxVar(data: TData)
	{
		this.viewContainerRef.clear();
		this.viewContainerRef.createEmbeddedView(this.templateRef, {
			$implicit: data,
		});
	}
}
