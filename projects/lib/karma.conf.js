if (typeof process.env.GITLAB_CI === 'undefined') {
	process.env.CHROMIUM_BIN = require('puppeteer').executablePath();
}

module.exports = function (config) {
	config.set({
		basePath: '',
		frameworks: ['jasmine', '@angular-devkit/build-angular'],
		plugins: [
			require('karma-jasmine'),
			require('karma-chrome-launcher'),
			require('karma-jasmine-html-reporter'),
			require('karma-coverage-istanbul-reporter'),
			require('@angular-devkit/build-angular/plugins/karma')
		],
		client: {
			clearContext: false // leave Jasmine Spec Runner output visible in browser
		},
		coverageIstanbulReporter: {
			dir: require('path').join(__dirname, '../../coverage/lib'),
			reports: ['html', 'lcovonly', 'text-summary'],
			fixWebpackSourcePaths: true
		},
		reporters: ['progress', 'kjhtml'],
		port: 9876,
		colors: false,
		logLevel: config.LOG_INFO,
		singleRun: true,
		browsers: ['Chrome', 'ChromiumHeadlessCI'],
		customLaunchers: {
			ChromiumHeadlessCI: {
				base: 'ChromiumHeadless',
				flags: ['--no-sandbox']
			}
		}
	});
};
